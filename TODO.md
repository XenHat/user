# TODO
TODO: Make synthwave i3 theme
TODO: Integrate wallpaper into loop_pscircle using proper wallpaper scaling
TODO: Write system service that blinks `phy0-led` (wifi light) on wireless adapter activity. Uses `xbacklight -ctrl phy0-led -set <n>`
TODO: Write guide to use ICECREAM/ICECC. NOTE: explain the parts with icecc-make-native, and CHOST match being required, etc
TODO: tweak outrun theme using htop as example to fix good/warn/bad gradients
